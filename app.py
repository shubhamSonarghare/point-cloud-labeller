# from multiprocessing.context import Process
import numpy as np
from pathlib import Path, PureWindowsPath
import pptk
from laspy.file import File
from pptk.points.points import load
from pynput.keyboard import Key, Listener
import pynput
import argparse
import os
import pandas as pd
import tkinter as tk
from tkinter import messagebox, ttk
import h5py
import time
import threading
import sys
import multiprocessing
# import glob

def save_pts(v, save_path):
    np.save(save_path, v)
    # print(v)

def save_class_dict(class_dict, save_path):
    np.save(save_path, class_dict)

def save_h5(key, arr, save_path):
    print(f'IN SAVE H5: {key}')
    with h5py.File(save_path, 'a') as f:
        dset = f[key]

        # if type(arr)==type([]):
        #     arr = np.array(arr)
        
        # present = np.in1d(arr, dset[()])
        # print(f'PRESET; {present}')
        # if np.any(present == True):
        #     print('SOME POINTS ALREADY PRESENT:')
        # arr = arr[np.logical_not(present)]
        dset.resize((len(arr),))
        dset[:] = arr.copy()

def add_h5_class(class_name, f):
    # with h5py.File(save_path, 'w') as f:
    f.create_dataset(class_name, data=[], dtype="i", maxshape=(None,))

def get_class_points(class_name, file):
    arr = []
    print(f'GETTING CLASS : {class_name}')
    with h5py.File(file, 'r') as f:
        dest = f[class_name]
        if type(dest) != type(None):
            arr = dest[:].copy()
    print(f'GETTING CLASS : {class_name}, {arr}')
    return list(arr)

def get_class(dict, val):
    # print(f'Getting classs of {val}')
    for key, value in dict.items():
        if val in value:
            return key
    return 0

def get_class_from_h5( val, f):
    # print(f'Getting classs of {val}')
    # with h5py.File(file_name, 'r') as f:
    for key in f.keys():
        dset = f[key]
        if key=='low_veg':
            if val in dset[()]:
                return key
    return 0

def del_from_h5(key, val, f):
    # with h5py.File(file_name, 'a') as f:
    dset = f[key]
    arr = list(dset[:])
    arr.remove(val)
    dset.resize((len(arr), ))
    dset[:] = arr

def show_info(message, title="Point Labeller"):
    root = tk.Tk()
    root.overrideredirect(1)
    root.withdraw()
    root.attributes("-topmost", True)
    messagebox.showinfo(title, message)
    root.destroy()

def create_tk():
    root = tk.Tk()
    root.overrideredirect(1)
    root.withdraw()
    root.attributes("-topmost", True)
    return root
# def reset_color(color_arr):
def show_progress_bar(root, title = 'Point Labeller', msg = "Writing to the file...."):
    # root.attributes("-topmost", True)
    # root.geometry('300x120')
    # root.title(title)
    # # # root.grid()
    # ft = ttk.Frame()
    # ft.pack(expand=True, fill=tk.BOTH, side=tk.TOP)
    # pb_hD = ttk.Progressbar(ft, orient='horizontal', mode='indeterminate')
    # pb_hD.pack(expand=True, fill=tk.BOTH, side=tk.TOP)
    # pb_hD.start(10)
    # root.mainloop()
    # root.overrideredirect(1)
    # root.withdraw()
    root.attributes("-topmost", True)
    root.geometry('480x80')
    root.title(title)
    root.grid()
    pb = ttk.Progressbar(
        root,
        orient='horizontal',
        mode='indeterminate',
        length=250
    )
    pb.grid(column=1, row=1, columnspan=2, padx=100, pady=0)

    label = ttk.Label(root, text=msg)
    label.grid(column=0, row=0, columnspan=2, padx=100, pady=10)

    pb.start()
    pb.step(10)
    root.mainloop()
    # return root, pb


if __name__ == "__main__":
    argP = argparse.ArgumentParser(description='USAGE app.py --i INPUT_POINT_CLOUD_PATH --l INPUT_LABEL_PATH --s DATASET_SAVE_PATH')

    argP.add_argument("--i", dest='points_path', required=True, help="input las path")
    # argP.add_argument("--l", dest='labels_path', required=True, help="input labels folder")
    argP.add_argument("--s", dest='save_path', required=True, help="input save folder")

    args = argP.parse_args()
    file_path = Path(args.points_path)
    save_path = Path(args.save_path)
    print(file_path.stem)
    # save_name = file_path.stem+'.npy'
    save_name = file_path.stem+'.h5'
    save_name = Path(args.save_path) / save_name

    if os.path.exists(save_path)==False:
        os.makedirs(save_path)
    # global tkroot
    

    ################################### Classes COlors ######################################

    class_color  = {
        'road': [189,0,189],
        'curb': [255,0,0],#[75,0,130],
        'tree': [0,255,0],
        'low_veg': [0, 129, 0],
        'pole': [0,20,255], 
        'signage_pole': [0,189,189],
        'lamp_pole':[138,43,226],
        'traffic_light_pole':[128,0,0],
        'gaurd_rail':[0,0,0],#[12,1,162]
        'fence':[128,0,0]
    }

    ########################################################################################



    global sel_pts, class_dict, current_class, current_color, cloud_color, current_points, inten_min, inten_max, arr
    current_color = None
    # class_dict = {key: [] for key in class_color.keys()}
    sel_pts = None
    load_saved_label = False
    if os.path.isfile(save_name):
        # sel_pts = np.load(save_name)
        print('Loading File')
        # class_dict = np.load(save_name, allow_pickle=True)
        # class_dict = class_dict.item()
        load_saved_label = True
        with h5py.File(save_name, 'r+') as f:
            classes = list(f.keys())
            for key in class_color:
                if key not in classes:
                    print(f"{'~'*100}Adding {key} Class to the file {'~'*100}")
                    add_h5_class(key, f)

    else:
        with h5py.File(save_name, 'w') as f:
            for key in class_color.keys():
                print(key)
                f.create_dataset(key, data=[], dtype="i", maxshape=(None,))

    # all_files = glob.glob(points_path / '*.las')
    # for file in all_files:
    
    if file_path.suffix == '.las': 
        lasfile = File(file_path, mode='r')
        arr = np.stack([lasfile.X, lasfile.Y, lasfile.Z, lasfile.Intensity], axis = -1)
        lasfile=None
    else:
        df = pd.read_csv(file_path)
        arr = df.to_numpy()

    # pyfile = Path('././data/Van_road_slices/Van__13.npy')
    # arr = np.load(pyfile)
     
    # pc = arr[:, 0:3]
    print('ARR SHAPEW: ', arr.shape)
    inten_min, inten_max = 0, 0
    if arr.shape[1] == 4:
        temp_col = arr[:,3]
        inten_min = temp_col.min()
        inten_max = temp_col.max()
        temp_col = (temp_col - temp_col.min())/(temp_col.max() - temp_col.min())
        # temp_col = (temp_col*255).astype(np.int32)
        temp_col = np.expand_dims(temp_col, axis=1)
        cloud_color = np.concatenate([temp_col, temp_col, temp_col], axis=-1)
        temp_col = None
        # arr = arr[:, 0:3]
        
        # cloud_color = np.ones((arr[:, 0:3].shape[0], 1), dtype=np.float16)*([128,128,128])
    else:
        cloud_color = np.ones((arr[:, 0:3].shape[0], 1), dtype=np.float16)*([128,128,128])
        cloud_color = cloud_color/255
    print(f'CLOUD COLOR SHAPE: {cloud_color.shape}')
    # v = pptk.viewer(pc)
    # v.set(point_size=10.0)
    # v.attributes(cloud_color / 255. )
    # cnt=0
    
    # if not(load_saved_label): 
    #     sel_pts = v.get('selected')
    # else:
    #     sel_pts = v.get('selected')
    #     for key in class_dict.keys():
    #         idx = class_dict[key]
    #         cloud_color[idx] = class_color[key]
    #     v.attributes(cloud_color/255.)
        # v.set(selected=sel_pts)
    sel_pts = []
    if (load_saved_label): 
        # sel_pts = v.get('selected')
        with h5py.File(save_name, 'r') as f:
            for key in f.keys():
                idx = f[key][()]
                
                print(idx)
                print(f'LEN {idx}: {len(idx)} || key: {key}')
                if len(idx)>1:
                    cloud_color[idx] = np.array(class_color[key])/255
                    

        # for key in class_dict.keys():
        #     idx = class_dict[key]
        #     cloud_color[idx] = class_color[key]
        # v.attributes(cloud_color/255.)

    pc = arr[:, 0:3]
    global point_size
    point_size = 12.0
    # v = pptk.viewer(pc, (cloud_color/255).astype(np.float16))
    v = pptk.viewer(pc, (cloud_color).astype(np.float16))
    v.set(point_size=point_size)
    
    # v.color_map(None, scale=[0, 255])
    # v.attributes((cloud_color/255).astype(np.float16))
    cnt=0
    current_points = None
    def on_keyPress(key):
        global sel_pts, current_color, class_dict, current_class, cloud_color, current_points
        global inten_min, inten_max, arr, point_size
        # current_points = None
        # tkroot = tk.Tk() 
        # tkroot.overrideredirect(1)
        # tkroot.withdraw()
        # tkroot.attributes("-topmost", True)
        print(key, key=='s', key==pynput.keyboard.KeyCode(char='s'))
        if (key == pynput.keyboard.KeyCode(char='s')):
            # print("points are: ")
            # print(v.get('selected'))
            sel_pts = v.get('selected')
            # print(f'Current Points IN SAVE : {current_points}')
            if (current_color!=None) and len(sel_pts)>1:
                # cloud_color[sel_pts] = current_color.copy()#[col/255. for col in current_color]
                def temp_save_function(root):
                    global cloud_color, current_points
                    cloud_color[sel_pts] = [col/255. for col in current_color]
                    # if current_class not in class_dict.keys():
                    #     class_dict[current_class] = []
                    # class_dict[current_class].extend(sel_pts)
                    current_points.extend(sel_pts)
                    print('Updating Class color!')
                    # v.reset()
                    # cloud_color = np.ones((arr[:, 0:3].shape[0], 1), dtype=np.float32)*([128,128,128])
                    # v.attributes()
                    # v.attributes((cloud_color/255).astype(np.float16))
                    v.attributes((cloud_color).astype(np.float16))
                    print('Updaing Finished!')
                    root.quit()
                this_root = tk.Tk()
                t1 = threading.Thread(target=temp_save_function, args=(this_root,))
                t1.start()
                show_progress_bar(this_root, msg='Saving in memory....')
                t1.join()
                this_root.destroy()
                print('THREAD Killed')

            else:
                print('Warning! No class selected')
                root = tk.Tk()
                messagebox.showwarning('Point Labeller', 'NO Classes selected!')
                root.destroy()


        if(key == pynput.keyboard.KeyCode(char='q')):
            v.set(selected=sel_pts)
            
        # if(key == pynput.keyboard.Key.delete):
        #     selection = v.get('selected')
        #     print('Deleting Selected points......')
        #     with h5py.File(save_name, 'a') as f:
        #         for idx in selection:
        #             # this_class = get_class(class_dict, idx)
        #             this_class = get_class_from_h5(idx, f)
        #             # cloud_color[idx] = [128/255,128/255,128/255]
        #             cc = (arr[idx, 3] - inten_min)/(inten_max - inten_min)
        #             cloud_color[idx] = [cc, cc, cc]
        #             if this_class!=0:
        #                 # class_dict[this_class].remove(idx)
        #                 del_from_h5(this_class, idx, f)
        #         v.set(selected = [])
        #         v.attributes((cloud_color).astype(np.float16))
        #     print('Selected points deleted!')

        if(key == pynput.keyboard.Key.delete):
            selection = v.get('selected')
            print('Deleting Selected points......')

            if len(selection)==0:
                show_info('No points selected!!', 'Point Labeller' )
            else:
                def del_core(root, selection):
                    selection = np.array(selection)
                    color_change_flag = False
                    with h5py.File(save_name, 'a') as f:
                        for key in f.keys():
                            dset = f[key]
                            print(key)
                            this_arr = dset[()].copy()
                            present = np.in1d(selection,this_arr )
                            if np.any(present == True):
                                ############## delete from h5 dataset ############
                                this_arr = np.sort(this_arr)
                                print(f'DEBUG DELETE: {present.shape, this_arr.shape}' )
                                tobedel = selection[present]
                                tobedel_idx = np.searchsorted(this_arr, tobedel)
                                print('RECOLORING')
                                color_change_flag = True
                                for each in tobedel:
                                    cc = (arr[each, 3] - inten_min)/(inten_max - inten_min)
                                    cloud_color[each] = [cc, cc, cc]
                                
                                this_arr = np.delete(this_arr, tobedel_idx)
                                dset.resize((len(this_arr), ))
                                dset[:] = this_arr
                                ########### del_from_selected array ##############
                                # selection = np.sort(selection)
                                # sel_del = selection[present]
                                # selection = np.delete(selection, np.searchsorted(selection, sel_del))

                                
                                
                                print(f'DELETED in {key}: {type(dset[()]),}')
                            # if key=='low_veg':
                            #     if val in dset[()]:
                            #         return key
                        v.set(selected = [])
                        if color_change_flag:
                            
                            v.attributes((cloud_color).astype(np.float16))
                    root.quit()

                this_tkroot = tk.Tk()
                t1=threading.Thread(target=del_core, args=(this_tkroot,selection))
                t1.start()
                show_progress_bar(this_tkroot, msg='Deleting Selected Points....')  # This will block while the mainloop runs
                this_tkroot.destroy()
                t1.join()
                print('THREAD KILLED')    

            
            print('Selected points deleted!')

        if(key == pynput.keyboard.KeyCode(char='w')):
            
            if current_color == None :
                    print("No new class-points saved")
                    # messagebox.showwarning('Point Labeller','No Class of the labels selected! Hence no points saved!!')
                    # tkroot.destroy()
                    show_info('No Class of the labels selected! Hence no points saved!!', 'Point Labeller' )
            else:
                def saving_func(root):
                    global current_points, cloud_color
                    sel_pts = v.get('selected')
                    # print(f'Selected points in W: {sel_pts}')
                    
                    if len(sel_pts)>0:
                        # cloud_color[sel_pts] = current_color
                        cloud_color[sel_pts] = [col/255. for col in current_color]

                        # class_dict[current_class].extend(sel_pts)
                        current_points.extend(sel_pts)
                        current_points = list(set(current_points))
                    # save_class_dict(class_dict, save_name)
                        save_h5(current_class, current_points, save_name)
                    # v.attributes()
                        v.attributes((cloud_color).astype(np.float16))
                        print(f"Written to the file: {save_name}")
                    root.quit()
                    # root.destroy()
                    # sys.exit()
                # save_pts(sel_pts, save_name)
                this_tkroot = tk.Tk()
                t1=threading.Thread(target=saving_func, args=(this_tkroot,))
                t1.start()
                show_progress_bar(this_tkroot)  # This will block while the mainloop runs
                this_tkroot.destroy()
                t1.join()
                print('THREAD KILLED')
            

        if(key== pynput.keyboard.KeyCode(char='x')):
            sel_pts = v.get('selected')
            wask = None
            if current_color == None:
                print("No new class saved")
                tkroot = create_tk()
                wask = messagebox.askquestion('Point Labeller', 'No new point selected/saved. You still want to exit?')
                tkroot.destroy()
            else:
                print('In Else')
                tkroot = create_tk()
                wask = messagebox.askquestion('Point Labeller', 'Are you sure want to save and exit?')
                tkroot.destroy()
                if wask == 'yes':
                    if len(sel_pts)>0:
                        cloud_color[sel_pts] = current_color
                        
                        # class_dict[current_class].extend(sel_pts)
                        current_points.extend(sel_pts)
                        current_points = list(set(current_points))
                # save_class_dict(class_dict, save_name)
                    save_h5(current_class, current_points, save_name)
            # save_pts(sel_pts, save_name)
            if wask != None and wask=='yes':
                return False
        
        if(key == pynput.keyboard.KeyCode(char='!')):
            current_class = 'road'
            current_color = class_color['road']
            current_points = get_class_points(current_class, save_name)
            if type(current_points) == type(None):
                current_points = []
            # messagebox.showinfo('Point Labeller', f'Current Class: {current_class}')
            # tkroot.destroy()
            show_info(f'Current Class: {current_class}')
        
        if(key == pynput.keyboard.KeyCode(char='"')):
            current_class = 'curb'
            current_color = class_color['curb']
            current_points = get_class_points(current_class, save_name)
            if type(current_points) == type(None):
                current_points = []
            # messagebox.showinfo('Point Labeller', f'Current Class: {current_class}')
            # tkroot.destroy()
            show_info(f'Current Class: {current_class}')
        
        if(key == pynput.keyboard.KeyCode(char='£')):
            current_class = 'tree'
            current_color = class_color['tree']
            current_points = get_class_points(current_class, save_name)
            if type(current_points) == type(None):
                current_points = []
            # messagebox.showinfo('Point Labeller', f'Current Class: {current_class}')
            # tkroot.destroy()
            show_info(f'Current Class: {current_class}')
        
        if(key == pynput.keyboard.KeyCode(char='$')):
            current_class = 'pole'
            current_color = class_color['pole']
            current_points = get_class_points(current_class, save_name)
            if type(current_points) == type(None):
                current_points = []
            # messagebox.showinfo('Point Labeller', f'Current Class: {current_class}')
            # tkroot.destroy()
            show_info(f'Current Class: {current_class}')

        if(key == pynput.keyboard.KeyCode(char='%')):
            current_class = 'signage_pole'
            current_color = class_color['signage_pole']
            current_points = get_class_points(current_class, save_name)
            if type(current_points) == type(None):
                current_points = []
            # messagebox.showinfo('Point Labeller', f'Current Class: {current_class}')
            # tkroot.destroy()
            show_info(f'Current Class: {current_class}')

        if(key == pynput.keyboard.KeyCode(char='^')):
            current_class = 'lamp_pole'
            current_color = class_color['lamp_pole']
            current_points = get_class_points(current_class, save_name)
            if type(current_points) == type(None):
                current_points = []
            # messagebox.showinfo('Point Labeller', f'Current Class: {current_class}')
            # tkroot.destroy()
            show_info(f'Current Class: {current_class}')

        if(key == pynput.keyboard.KeyCode(char='&')):
            current_class = 'traffic_light_pole'
            current_color = class_color['traffic_light_pole']
            current_points = get_class_points(current_class, save_name)
            if type(current_points) == type(None):
                current_points = []
            # messagebox.showinfo('Point Labeller', f'Current Class: {current_class}')
            # tkroot.destroy()
            show_info(f'Current Class: {current_class}')
        
        if(key == pynput.keyboard.KeyCode(char='*')):
            current_class = 'low_veg'
            current_color = class_color['low_veg']
            current_points = get_class_points(current_class, save_name)
            if type(current_points) == type(None):
                current_points = []
            # messagebox.showinfo('Point Labeller', f'Current Class: Low Vegetation')
            # tkroot.destroy()
            show_info(f'Current Class: Low Vegetation')
        
        if(key == pynput.keyboard.KeyCode(char='(')):
            current_class = 'gaurd_rail'
            current_color = class_color['gaurd_rail']
            current_points = get_class_points(current_class, save_name)
            if type(current_points) == type(None):
                current_points = []
            # messagebox.showinfo('Point Labeller', f'Current Class: Gaurd_rail')
            # tkroot.destroy()
            show_info(f'Current Class: Gaurd_rail')
        
        if(key == pynput.keyboard.KeyCode(char=')')):
            current_class = 'fence'
            current_color = class_color['fence']
            current_points = get_class_points(current_class, save_name)
            if type(current_points) == type(None):
                current_points = []
            # messagebox.showinfo('Point Labeller', f'Current Class: Gaurd_rail')
            # tkroot.destroy()
            show_info(f'Current Class: Fence')

        if(key == pynput.keyboard.KeyCode(char='=')):
            print('Increasing point size')
            point_size = point_size+1.0
            v.set(point_size = point_size)

        
        if(key == pynput.keyboard.KeyCode(char='-')):
            point_size = point_size-1.0
            print(f'Point size: {point_size}')
            v.set(point_size = point_size)


        if(key == pynput.keyboard.KeyCode(char='h')):
            # messagebox.showinfo('Point Cloud Labels', f'Road: ! [shift+1] \nCurb: " [shift+2] \
            #                     \nTree: £ [shift+3] \nPole: $ [shift+4] \nsignage_pole: % [shift+5] \
            #                     \nlamp_pole: ^ [shift+6] \ntraffic_light_pole: & [shift+7] \
            #                     \nLow Vegetation: *[shift+8]\
            #                     \nGaurd Rail: ( [shift+9]')
            # tkroot.destroy()
            show_info(f'Road: ! [shift+1] \nCurb: " [shift+2] \
                        \nTree: £ [shift+3] \nPole: $ [shift+4] \nsignage_pole: % [shift+5] \
                        \nlamp_pole: ^ [shift+6] \ntraffic_light_pole: & [shift+7] \
                        \nLow Vegetation: *[shift+8]\
                        \nGaurd Rail: ( [shift+9]\
                        \nFence: ) [shift+0]' )

    
    
    with Listener(on_release = on_keyPress) as listener:
        listener.join()
        

    v.close()
